echo "[x] Update packages"
apt update
echo "[x] Install Software Properties Common package"
apt install software-properties-common -y
echo "[x] Add Ansible repository"
apt-add-repository ppa:ansible/ansible -y
echo "[x] Update packages"
apt update
echo "[x] Install Ansible"
apt install ansible -y
echo "[x] Ansible has been installed"
ansible --version